/*
 * Copyright (C) 2019 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version. Please refer to 
 * You may obtain a copy of the License at
 *
 *      https://opensource.org/licenses/lgpl-3.0.html
 *
 */
package com.github.mcheung63.netbeans.minimap;

import hk.quantr.peterswing.CommonLib;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Transparency;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.List;
import javax.swing.JEditorPane;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.StyleConstants;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.api.editor.settings.FontColorSettings;
import org.netbeans.modules.editor.lib2.highlighting.HighlightingManager;
import org.netbeans.spi.editor.highlighting.HighlightsContainer;
import org.netbeans.spi.editor.highlighting.HighlightsSequence;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.openide.util.RequestProcessor;

public class MinimapPanel extends JPanel implements ActionListener, MouseListener, MouseMotionListener {

    JTextComponent jtc;
    Document document;
    BufferedImage codeImage;
    BufferedImage codeImageScaled;
    BufferedImage highLightAreaImage;
    JScrollPane jScrollPane;
    int startLine;
    int endLine;

    int blockHeight;
    int margin = NbPreferences.forModule(MinimapPanel.class).getInt("margin", 1);
    int scale = NbPreferences.forModule(MinimapPanel.class).getInt("scale", 1);
    boolean ignoreMiddleWhiteSpace = NbPreferences.forModule(MinimapPanel.class).getBoolean("middleWhiteSpace", true);
    boolean showCurrentLine = NbPreferences.forModule(MinimapPanel.class).getBoolean("showCurrentLine", true);
    boolean showLines = NbPreferences.forModule(MinimapPanel.class).getBoolean("showLines", true);
    boolean highlightOccurrence = NbPreferences.forModule(MinimapPanel.class).getBoolean("highlightOccurrence", true);
    boolean greyColor = NbPreferences.forModule(MinimapPanel.class).getBoolean("greyColor", false);
    int noOfLineLowRes = Integer.parseInt(NbPreferences.forModule(MinimapPanel.class).get("noOfLineLowRes", "1000"));
    JScrollBar verticalScrollBar;
    JPopupMenu jPopupMenu = new JPopupMenu();
    double ratio;
    boolean running1 = false;
    JMenuItem item = new JMenuItem("Setting");
    JMenuItem item2 = new JMenuItem("Toggle minimap");
    JMenuItem item3 = new JMenuItem("Refresh");
    List<Integer> bookmarkList;
    int noOfLine;
//	boolean isRunning;
    RequestProcessor rp;
    RequestProcessor.Task lastRefreshTask;
    GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice device = env.getDefaultScreenDevice();
    GraphicsConfiguration config = device.getDefaultConfiguration();
    Color highlightColor = getHighlightColor();
    int lastLineNo;

    MinimapPanel(final JTextComponent jtc) {
        super();

        this.jtc = jtc;
        setBackgroundColor();

        this.document = jtc.getDocument();
        jScrollPane = (JScrollPane) jtc.getParent().getParent().getParent();
        verticalScrollBar = jScrollPane.getVerticalScrollBar();
        jPopupMenu.add(item);
        item.addActionListener(this);
        jPopupMenu.add(item2);
        item2.addActionListener(this);
        jPopupMenu.add(item3);
        item3.addActionListener(this);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.setComponentPopupMenu(jPopupMenu);

        jScrollPane.getViewport().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (!MenuAction.visible) {
                    return;
                }
                if (jtc.getText().length() > 0) {
                    JViewport viewport = (JViewport) e.getSource();
                    Rectangle viewRect = viewport.getViewRect();

                    Point p = viewRect.getLocation();
                    int startIndex = jtc.viewToModel(p);

                    int lineHeight = jtc.getFontMetrics(jtc.getFont()).getHeight();
                    p.y += viewRect.height - lineHeight;
                    int endIndex = jtc.viewToModel(p);

                    Element root = jtc.getDocument().getDefaultRootElement();
                    startLine = root.getElementIndex(startIndex);
                    endLine = root.getElementIndex(endIndex);

                    generateImage(false);
                    repaint();
                }
            }
        });
        document.addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                try {
                    int lineNo = getLineOfOffset(jtc, jtc.getCaretPosition());

                    if (lastLineNo != lineNo) {
                        generateImage(true);
                        lastLineNo = lineNo;
                    } else {
                        generateImage(false);
                    }
                } catch (BadLocationException ex) {
                    Exceptions.printStackTrace(ex);
                }

                repaint();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                generateImage(true);
                repaint();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });

        jtc.addCaretListener(new CaretListener() {
            @Override
            public void caretUpdate(CaretEvent e) {
                generateImage(false);
                repaint();
            }
        });
        init();
//		Timer timer = new Timer(1000, new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				generateImage(regen);
//				//repaint();
//			}
//		});
//		timer.start();
//		rp = new RequestProcessor(MinimapPanel.class);
    }

    public void init() {
        if (jtc.getText().length() > 0) {
            JViewport viewport = jScrollPane.getViewport();
            Rectangle viewRect = viewport.getViewRect();

            Point p = viewRect.getLocation();
            int startIndex = jtc.viewToModel(p);

            int lineHeight = jtc.getFontMetrics(jtc.getFont()).getHeight();
            p.y += viewRect.height - lineHeight;
            int endIndex = jtc.viewToModel(p);

            Element root = jtc.getDocument().getDefaultRootElement();
            startLine = root.getElementIndex(startIndex);
            endLine = root.getElementIndex(endIndex);

            generateImage(true);
            repaint();
        }
    }

    public void paint(Graphics g) {
        if (!MenuAction.visible) {
            return;
        }
        super.paint(g);
        try {
            if (codeImage != null /*&& highLightAreaImage != null*/) {
                if (noOfLine > noOfLineLowRes) {
                    if (codeImageScaled == null) {
                        if (jScrollPane.getVerticalScrollBar().isVisible()) {
                            if (codeImage.getHeight() > getHeight()) {
                                codeImageScaled = Helper.resize(codeImage, getWidth(), getHeight());
                                ratio = (double) codeImage.getHeight() / getHeight();
                            } else {
                                codeImageScaled = Helper.resize(codeImage, getWidth(), codeImage.getHeight());
                                ratio = (double) codeImage.getHeight() / codeImage.getHeight();
                            }
                        } else {
                            if (codeImage.getHeight() <= getHeight()) {
                                codeImageScaled = Helper.resize(codeImage, getWidth(), codeImage.getHeight());
                                ratio = (double) codeImage.getHeight() / codeImage.getHeight();
                            } else {
                                int drawImageHeight = codeImage.getHeight() / (codeImage.getWidth() / getWidth());
                                if (drawImageHeight <= getHeight()) {
                                    codeImageScaled = Helper.resize(codeImage, getWidth(), drawImageHeight);
                                    ratio = (double) codeImage.getHeight() / drawImageHeight;
                                } else {
                                    codeImageScaled = Helper.resize(codeImage, codeImage.getWidth() / (codeImage.getHeight() / getHeight()), codeImage.getHeight());
                                    ratio = (double) codeImage.getHeight() / codeImage.getHeight();
                                }
                            }
                        }
                    }
                    g.drawImage(codeImageScaled, 0, 0, codeImageScaled.getWidth(), codeImageScaled.getHeight(), null);
                } else {
                    if (jScrollPane.getVerticalScrollBar().isVisible()) {
                        if (codeImage.getHeight() > getHeight()) {
                            g.drawImage(codeImage, 0, 0, getWidth(), getHeight(), null);
//							g.drawImage(highLightAreaImage, 0, 0, getWidth(), getHeight(), null);
                            ratio = (double) codeImage.getHeight() / getHeight();
                        } else {
                            g.drawImage(codeImage, 0, 0, getWidth(), codeImage.getHeight(), null);
//							g.drawImage(highLightAreaImage, 0, 0, getWidth(), highLightAreaImage.getHeight(), null);
                            ratio = (double) codeImage.getHeight() / codeImage.getHeight();
                        }
                    } else {
                        if (codeImage.getHeight() <= getHeight()) {
                            g.drawImage(codeImage, 0, 0, getWidth(), codeImage.getHeight(), null);
//							g.drawImage(highLightAreaImage, 0, 0, getWidth(), highLightAreaImage.getHeight(), null);
                            ratio = (double) codeImage.getHeight() / codeImage.getHeight();
                        } else {
                            int drawImageHeight = codeImage.getHeight() / (codeImage.getWidth() / getWidth());
                            if (drawImageHeight <= getHeight()) {
                                g.drawImage(codeImage, 0, 0, getWidth(), drawImageHeight, null);
//								g.drawImage(highLightAreaImage, 0, 0, getWidth(), drawImageHeight, null);
                                ratio = (double) codeImage.getHeight() / drawImageHeight;
                            } else {
                                g.drawImage(codeImage, 0, 0, codeImage.getWidth() / (codeImage.getHeight() / getHeight()), codeImage.getHeight(), null);
//								g.drawImage(highLightAreaImage, 0, 0, highLightAreaImage.getWidth() / (highLightAreaImage.getHeight() / getHeight()), highLightAreaImage.getHeight(), null);
                                ratio = (double) codeImage.getHeight() / codeImage.getHeight();
                            }
                        }
                    }
                }
                g.setColor(highlightColor);
                g.fillRect(0, (int) (startLine * (blockHeight + margin) / ratio), codeImage.getWidth(), (int) ((endLine - startLine) * (blockHeight + margin) / ratio));
            }
        } catch (Exception ex) {
        }
    }

    boolean isBookmarkLine(int lineNo) {
        if (bookmarkList == null) {
            return false;
        }
        for (Integer tempLineNo : bookmarkList) {
            if (tempLineNo == lineNo) {
                return true;
            }
        }
        return false;
    }

    void generateImage(boolean needRegen) {
        if (!MenuAction.visible) {
            return;
        }
        if (codeImage == null || needRegen) {
            codeImage = generateCodeMap();
            codeImageScaled = null;
        }
        //generateHighlight();
    }

    //Graphics2D imageGHightlight;
    /*void generateHighlight() {
		if (codeImage != null) {
			if (imageGHightlight == null || highLightAreaImage == null) {
				//highLightAreaImage = new BufferedImage(codeImage.getWidth(), codeImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
				highLightAreaImage = config.createCompatibleImage(codeImage.getWidth(), codeImage.getHeight(), Transparency.TRANSLUCENT);
				imageGHightlight = (Graphics2D) highLightAreaImage.getGraphics();
				imageGHightlight.setBackground(new Color(255, 255, 255, 0));
			}
			//ModuleLib.log("imageG=" + imageG + ", startLine=" + startLine);
			if (imageGHightlight != null) {
				imageGHightlight.clearRect(0, 0, highLightAreaImage.getWidth(), highLightAreaImage.getHeight());
				drawHighlightArea(imageGHightlight, (blockHeight + margin) * startLine, (blockHeight + margin) * (endLine - startLine + 1), highLightAreaImage.getWidth());
			}
		}
	}*/
//	void drawHighlightArea(Graphics2D g, int y, int height, int width) {
//		g.setColor(highlightColor);
//		g.fillRect(0, y, width, height);
//	}
    BufferedImage generateCodeMap() {
        try {
            /*if (bookmarkList == null) {
				try {
					bookmarkList = new ArrayList<Integer>();
					Project project = NetbeansUtil.getProject();
					if (project != null) {
						AuxiliaryConfiguration config = ProjectUtils.getAuxiliaryConfiguration(project);
						org.w3c.dom.Element element = config.getConfigurationFragment("editor-bookmarks", "http://www.netbeans.org/ns/editor-bookmarks/2", false);
						if (element != null) {
							NodeList list = element.getElementsByTagNameNS("http://www.netbeans.org/ns/editor-bookmarks/2", "bookmark");
							for (int x = 0; x < list.getLength(); x++) {
								org.w3c.dom.Element enEl = (org.w3c.dom.Element) list.item(x);
								bookmarkList.add(Integer.parseInt(enEl.getTextContent().trim()));
							}
						}
					}
				} catch (Exception ex) {
					ModuleLib.log(CommonLib.printException(ex));
				}
			}

			String bookmarkColorStr = NbPreferences.forModule(MinimapPanel.class).get("bookmrkColor", null);
			Color bookmarkColor;
			if (bookmarkColorStr == null) {
				bookmarkColor = Color.CYAN;
			} else {
				bookmarkColor = Color.decode(bookmarkColorStr);
			}*/
            if (jtc == null) {
                return null;
            }
            JEditorPane editorPane = (JEditorPane) jtc;
            if (editorPane == null) {
                return null;
            }
            Lookup lookup = MimeLookup.getLookup(editorPane.getContentType());
            FontColorSettings settings = lookup.lookup(FontColorSettings.class);
            Color defaultColor = (Color) settings.getTokenFontColors("default").getAttribute(StyleConstants.Foreground);

            String lines[] = jtc.getText().split("\n");
            noOfLine = lines.length;
            if (noOfLine < 1000) {
                blockHeight = 5;
            } else if (noOfLine < 1500) {
                blockHeight = 4;
            } else if (noOfLine < 2000) {
                blockHeight = 3;
            } else {
                blockHeight = 1;
            }

            int longestLined = Integer.MIN_VALUE;
            int averageLineLength = 0;
            for (int x = 0; x < lines.length; x++) {
                int lineLength = getLineWidth(lines[x]);
                averageLineLength += lineLength;
                if (lineLength > longestLined) {
                    longestLined = lineLength;
                }
            }
            if (lines.length > 0) {
                averageLineLength /= lines.length;
                if (longestLined > averageLineLength * 10) {
                    longestLined = averageLineLength * 10;
                }
            }
            if (noOfLine == 0 || longestLined <= 0) {
                return new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
            }

            BufferedImage tempImage = config.createCompatibleImage(longestLined, noOfLine * (blockHeight + margin), Transparency.TRANSLUCENT);

            //final BufferedImage tempImage = new BufferedImage(longestLined, noOfLine * (blockHeight + margin), BufferedImage.TYPE_INT_ARGB);
            Graphics2D imageG = (Graphics2D) tempImage.getGraphics();

            // draw every lines
            if (showLines) {
                int offsetX = 0;
                int offsetY = 0;
                Color lastColor = null;
                int lineNo = 0;
                HighlightsContainer hc = HighlightingManager.getInstance(jtc).getBottomHighlights();
                HighlightsSequence hq = hc.getHighlights(0, document.getLength());

                while (hq.moveNext()) {
                    AttributeSet as = hq.getAttributes();

                    for (int x = 0; x < hq.getEndOffset() - hq.getStartOffset(); x++) {
                        String text = document.getText(hq.getStartOffset() + x, 1);

                        Color fc;
                        if (text.equals(" ") || text.equals("\t")) {
                            if (ignoreMiddleWhiteSpace) {
                                fc = lastColor;
                            } else {
                                fc = null;
                            }
                        } else if (text.equals("\n")) {
                            offsetX = 0;
                            offsetY += blockHeight + margin;
                            lineNo++;
                            lastColor = null;
                            continue;
                        } else {
                            /*if (isBookmarkLine(lineNo)) {
								fc = bookmarkColor;
							} else*/ if (as == null || as.getAttribute(StyleConstants.Foreground) == null) {
                                fc = greyColor ? Color.gray : defaultColor;
                            } else {
                                fc = greyColor ? Color.gray : (Color) as.getAttribute(StyleConstants.Foreground);
                            }
                        }
                        lastColor = fc;

                        if (fc != null) {
                            imageG.setColor(fc);
                            float realScale = 1;
                            if (scale > 1 && getLineWidth(lines[lineNo]) > 0) {
                                realScale = (longestLined / getLineWidth(lines[lineNo]));
                            }
                            //float blockDrawWidth = blockWidth * scale * realScale;
                            if (isBookmarkLine(lineNo)) {
                                imageG.fillRect(0, offsetY, longestLined, blockHeight);
                            } else {
                                float blockDrawWidth = getLineWidth(text) * scale * realScale;
                                imageG.fillRect(offsetX, offsetY, (int) blockDrawWidth, blockHeight);
                            }
                        }

                        offsetX += getLineWidth(text);
                    }
                }
            }

            /*if (highlightOccurrence) {
//				ArrayList<Lookup> lookups = new ArrayList<Lookup>();
//				for (MimePath mimePath : mimePaths) {
//					lookups.add(MimeLookup.getLookup(mimePath));
//				}
//				ProxyLookup lookup = new ProxyLookup(
//						lookups.toArray(new Lookup[lookups.size()]));
				Lookup.Result<HighlightsLayerFactory> factories = lookup.lookup(new Lookup.Template<HighlightsLayerFactory>(HighlightsLayerFactory.class));
				//ModuleLib.log("factories=" + factories);
				Collection<? extends HighlightsLayerFactory> all = factories.allInstances();
				for (HighlightsLayerFactory fac: all){
					ModuleLib.log("fac = " + fac);
				}


//				HashMap<String, HighlightsLayer> layers = new HashMap<String, HighlightsLayer>();
//				Iterator it = layers.entrySet().iterator();
//				while (it.hasNext()) {
//					Map.Entry pair = (Map.Entry) it.next();
//					ModuleLib.log(pair.getKey() + " = " + pair.getValue());
//				}
//				HighlightsLayerFactory.Context context = HighlightingSpiPackageAccessor.get().createFactoryContext(document, jtc);
//
//				for (HighlightsLayerFactory factory : all) {
//					HighlightsLayer[] factoryLayers = factory.createLayers(context);
//					if (factoryLayers == null) {
//						continue;
//					}
//
//					for (HighlightsLayer layer : factoryLayers) {
//						HighlightsLayerAccessor layerAccessor = HighlightingSpiPackageAccessor.get().getHighlightsLayerAccessor(layer);
//
//						String layerTypeId = layerAccessor.getLayerTypeId();
//						ModuleLib.log(layerTypeId + " = " + layerAccessor);
//						if (!layers.containsKey(layerTypeId)) {
//							layers.put(layerTypeId, layer);
//						}
//					}
//				}
			}*/
            // draw every lines
            if (showCurrentLine) {
                int tempLineNo = getLineOfOffset(jtc, jtc.getCaretPosition());
                imageG.setColor(new Color(255, 0, 0, 120));
                imageG.fillRect(0, tempLineNo * (blockHeight + margin), tempImage.getWidth(), blockHeight);
            }

            //Fold
            /*drawHighlightArea(imageG, (blockHeight + margin) * startLine, (blockHeight + margin) * (endLine - startLine + 1), tempImage.getWidth());
			FoldHierarchy hierarchy = FoldHierarchy.get(jtc);
			Fold rootFold = hierarchy.getRootFold();
			int foldCount = rootFold.getFoldCount();
			for (int i = 0; i < foldCount; i++) {
				Fold childFold = rootFold.getFold(i);
				ModuleLib.log(i + " = " + childFold);
			}*/
            //end Fold
            imageG.dispose();
            return tempImage;
        } catch (BadLocationException ex) {
            ModuleLib.log(CommonLib.printException(ex));
            return null;
        }
    }

    static int getLineOfOffset(JTextComponent comp, int offset) throws BadLocationException {
        Document doc = comp.getDocument();
        if (offset < 0) {
            throw new BadLocationException("Can't translate offset to line", -1);
        } else if (offset > doc.getLength()) {
            throw new BadLocationException("Can't translate offset to line", doc.getLength() + 1);
        } else {
            Element map = doc.getDefaultRootElement();
            return map.getElementIndex(offset);
        }
    }

    static int getLineStartOffset(JTextComponent comp, int line) throws BadLocationException {
        Element map = comp.getDocument().getDefaultRootElement();
        if (line < 0) {
            throw new BadLocationException("Negative line", -1);
        } else if (line >= map.getElementCount()) {
            throw new BadLocationException("No such line", comp.getDocument().getLength() + 1);
        } else {
            Element lineElem = map.getElement(line);
            return lineElem.getStartOffset();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == item) {
            SettingDialog settingDialog = new SettingDialog(null, true, jtc);
            settingDialog.setLocationRelativeTo(null);
            settingDialog.setVisible(true);
            JPanel panel = (JPanel) jtc.getParent().getParent().getParent().getParent();
            this.setPreferredSize(new Dimension(Integer.parseInt(settingDialog.widthSpinner.getValue().toString()), getHeight()));
            panel.updateUI();
            margin = NbPreferences.forModule(MinimapPanel.class).getInt("margin", 1);
            scale = NbPreferences.forModule(MinimapPanel.class).getInt("scale", 1);
            ignoreMiddleWhiteSpace = NbPreferences.forModule(MinimapPanel.class).getBoolean("middleWhiteSpace", true);
            showCurrentLine = NbPreferences.forModule(MinimapPanel.class).getBoolean("showCurrentLine", true);
            showLines = NbPreferences.forModule(MinimapPanel.class).getBoolean("showLines", true);
            highlightOccurrence = NbPreferences.forModule(MinimapPanel.class).getBoolean("highlightOccurrence", true);
            greyColor = NbPreferences.forModule(MinimapPanel.class).getBoolean("greyColor", false);
            noOfLineLowRes = Integer.parseInt(NbPreferences.forModule(MinimapPanel.class).get("noOfLineLowRes", "1000"));
            //highlightColor = Color.decode(NbPreferences.forModule(MinimapPanel.class).get("highlightColor", null));
            highlightColor = getHighlightColor();
            setBackgroundColor();
            generateImage(true);
            repaint();
        } else if (e.getSource() == item2) {
            new MenuAction().actionPerformed(null);
        } else if (e.getSource() == item3) {
            generateImage(true);
            repaint();
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        int rowNo = (int) (e.getY() / (blockHeight + margin) * ratio);
        int lineHeight = jtc.getFontMetrics(jtc.getFont()).getHeight();

        jtc.scrollRectToVisible(new Rectangle(0, (int) jtc.getSize().getHeight(), 100, 100));
        int temp = getHeight() / lineHeight;
        jtc.scrollRectToVisible(new Rectangle(0, (rowNo - (temp / 2)) * lineHeight, 100, 100));
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            int rowNo = (int) (e.getY() / (blockHeight + margin) * ratio);
//			ModuleLib.log("blockHeight=" + blockHeight);
//			ModuleLib.log("margin=" + margin);
//			ModuleLib.log("ratio=" + ratio);
//			ModuleLib.log("rowNo=" + rowNo);
            int lineHeight = jtc.getFontMetrics(jtc.getFont()).getHeight();

            jtc.scrollRectToVisible(new Rectangle(0, (int) jtc.getSize().getHeight(), 100, 100));
            int temp = getHeight() / lineHeight;
            jtc.scrollRectToVisible(new Rectangle(0, (rowNo - (temp / 2)) * lineHeight, 100, 100));
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    private void setBackgroundColor() {
        Color backgroundColor;
        String colorStr = NbPreferences.forModule(MinimapPanel.class).get("backgroundColor", null);
        if (colorStr == null) {
            backgroundColor = jtc.getBackground();
        } else {
            backgroundColor = Color.decode(colorStr);
        }
        setBackground(backgroundColor);
    }

    int getLineWidth(String line) {
        int len = 0;
        for (int x = 0; x < line.length(); x++) {
            if (line.charAt(x) == '\t') {
                len += 28;
            } else {
                len += 7;
            }
        }
        return len;
    }

    private MimePath[] getAllDocumentMimePath() {
        Document doc = jtc.getDocument();
        String mainMimeType;

        Object propMimeType = doc.getProperty("mimeType");
        if (propMimeType != null) {
            mainMimeType = propMimeType.toString();
        } else {
            mainMimeType = jtc.getUI().getEditorKit(jtc).getContentType();
        }

        return new MimePath[]{MimePath.parse(mainMimeType)};
    }

    private Color getHighlightColor() {
        String colorStr = NbPreferences.forModule(MinimapPanel.class).get("highlightColor", null);
        if (colorStr == null) {
            return new Color(0, 0, 255, 80);
        }
        Color color = Color.decode(colorStr);
        if (color == null) {
            return new Color(0, 0, 255, 80);
        }
        color = new Color(color.getRed(), color.getGreen(), color.getBlue(), 80);
        return color;
    }
}
